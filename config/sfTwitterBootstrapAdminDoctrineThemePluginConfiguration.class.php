<?php

/**
 * This file is part of the sfTwitterBootstrapAdminDoctrineThemePlugin Eco System
 */
class sfTwitterBootstrapAdminDoctrineThemePluginConfiguration extends sfPluginConfiguration {

    public function initialize() {
        sfConfig::set("sf_admin_module_web_dir", "/sfTwitterBootstrapAdminDoctrineThemePlugin");
    }

}