[?php if ($value): ?]
    <i class="icon-ok" title="[?php echo __('Checked', array(), 'sf_admin') ?]"></i>
[?php else: ?]
    &nbsp;
[?php endif; ?]
