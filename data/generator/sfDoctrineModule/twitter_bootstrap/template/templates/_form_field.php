[?php if ($field->isPartial()): ?]
    [?php include_partial('<?php echo $this->getModuleName() ?>/'.$name, array('form' => $form, 'attributes' => $attributes instanceof sfOutputEscaper ? $attributes->getRawValue() : $attributes)) ?]
[?php elseif ($field->isComponent()): ?]
    [?php include_component('<?php echo $this->getModuleName() ?>', $name, array('form' => $form, 'attributes' => $attributes instanceof sfOutputEscaper ? $attributes->getRawValue() : $attributes)) ?]
[?php else: ?]
    
    <div class="control-group [?php if($form[$name]->hasError()): ?]error[?php endif ?]">
        [?php echo $form[$name]->renderLabel($label, array('class' => 'control-label')) ?]

        <div class="controls">
            
            [?php echo $form[$name]->render(array('class' => 'span6')) ?]
            
            [?php if($form[$name]->hasError()): ?]
                <span class="help-inline">[?php echo $form[$name]->getError() ?]</span>
            [?php endif ?]
            
            [?php if ($help): ?]
                <div class="alert alert-info">[?php echo __($help, array(), '<?php echo $this->getI18nCatalogue() ?>') ?]</div>
            [?php elseif ($help = $form[$name]->renderHelp()): ?]
                <div class="alert alert-info">[?php echo $help ?]</div>
            [?php endif; ?]
        </div>

        
    </div>
[?php endif; ?]
